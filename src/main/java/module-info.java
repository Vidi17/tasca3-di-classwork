module es.vidal.tasca3diclasswork {
    requires javafx.controls;
    requires javafx.fxml;


    opens es.vidal.tasca3diclasswork to javafx.fxml;
    exports es.vidal.tasca3diclasswork;
}
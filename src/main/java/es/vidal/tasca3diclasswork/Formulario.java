package es.vidal.tasca3diclasswork;

public class Formulario {

    private String nombre;
    private String apellidos;
    private String comentario;
    private String genero;
    private String ciudad;
    private String so;
    private String horas;
    private String fecha;

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public void setHoras(String horas) {
        this.horas = horas;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getComentario() {
        return comentario;
    }

    public String getGenero() {
        return genero;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getSo() {
        return so;
    }

    public String getHoras() {
        return horas;
    }

    public String getFecha() {
        return fecha;
    }
}

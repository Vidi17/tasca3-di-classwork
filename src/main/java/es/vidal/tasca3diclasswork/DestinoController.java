package es.vidal.tasca3diclasswork;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class DestinoController implements Initializable {

    @FXML
    Pane raiz;

    @FXML
    private Label nombre, apellidos, comentario, genero, ciudad, so, horas, fecha;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    @FXML
    public void recuperarDatos(){
        Stage stage = (Stage) this.raiz.getScene().getWindow();
        Formulario formulario = (Formulario) stage.getUserData();

        nombre.setText(String.format("Nombre: %s", formulario.getNombre()));
        apellidos.setText(String.format("Apellidos: %s", formulario.getApellidos()));
        comentario.setText(String.format("Comentario: %s", formulario.getComentario()));
        genero.setText(String.format("Genero: %s", formulario.getGenero()));
        ciudad.setText(String.format("Ciudad: %s", formulario.getCiudad()));
        so.setText(String.format("Sistema Operativo: %s", formulario.getSo()));
        horas.setText(String.format("Horas delante del PC: %s", formulario.getHoras()));
        fecha.setText(String.format("Fecha: %s", formulario.getFecha()));
    }
}

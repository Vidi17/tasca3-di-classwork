package es.vidal.tasca3diclasswork;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class DatosController implements Initializable {

    @FXML
    TextField nombre, apellidos;

    @FXML
    TextArea comentario;

    @FXML
    RadioButton masculino, femenino, otros;

    @FXML
    ChoiceBox ciudad, sistemaOperativo;

    @FXML
    Spinner horasOrdenador;

    @FXML
    DatePicker fecha;

    @FXML
    private void enviarDatos(ActionEvent event){
        Formulario formulario = new Formulario();

        String nombreString = nombre.getText().trim();
        String apellidosString = apellidos.getText();
        String comentarioString = comentario.getText();
        String genero;

        if (masculino.isSelected()){
            genero = "Masculino";
        }else if (femenino.isSelected()){
            genero = "Femenino";
        }else {
            genero = "Otro";
        }

        String ciudadString = ciudad.getValue().toString();
        String so = sistemaOperativo.getValue().toString();
        String horas = horasOrdenador.getValue().toString();
        String fechaDate = fecha.getValue().toString();

        formulario.setNombre(nombreString);
        formulario.setApellidos(apellidosString);
        formulario.setComentario(comentarioString);
        formulario.setGenero(genero);
        formulario.setCiudad(ciudadString);
        formulario.setSo(so);
        formulario.setHoras(horas);
        formulario.setFecha(fechaDate);

        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
        try {
            Parent root = FXMLLoader.load(getClass().getResource("destino.fxml"));
            stage.setUserData(formulario);
            Scene scene = new Scene(root);
            stage.setTitle("Formulario");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            System.err.printf("Error creando ventana: %s%n", e.getMessage());
            System.err.println(e.getCause());
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}

package es.vidal.tasca3diclasswork;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;


public class PrincipalController {
    @FXML
    private TextField usuario;

    @FXML
    private Label label;

    @FXML
    protected void abrirLogin() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("login.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.DECORATED);
            stage.setTitle("Login");
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    protected void iniciarSesion() {
        String usuarioString = usuario.getText();
        label.setText(usuarioString);

    }

    @FXML
    protected void abrirCalculadora() {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("calculadora.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();

    }

    @FXML
    protected void abrirFormulario() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("formulario.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.DECORATED);
            stage.setTitle("Login");
            stage.setScene(new Scene(root1));

            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}